<?php

namespace AppBundle\Util;

class MoneyConverter
{
    /**
     * Convert string value of money to internal value
     *
     * @param string $value `"500.5010"`
     * @return int `50000`
     */
    public static function stringToInternal(string $value): int
    {
        $value = floatval($value);
        $value = round($value, 2) * 100;
        $value = intval($value);

        return $value;
    }

    /**
     * Convert float value of money to internal value
     *
     * @param float $value `500.5010`
     * @return int `50050`
     */
    public static function floatToInternal(float $value): int
    {
        $value = round($value, 2) * 100;
        $value = intval($value);

        return $value;
    }

    /**
     * Convert integer part of money to internal value
     *
     * @param int $value `500`
     * @return int `50000`
     */
    public static function intToInternal(int $value): int
    {
        $value = $value * 100;

        return $value;
    }

    /**
     * @param int $value
     * @return float
     */
    public static function internalToFloat(int $value): float
    {
        $value = floatval($value / 100);

        return $value;
    }
}
