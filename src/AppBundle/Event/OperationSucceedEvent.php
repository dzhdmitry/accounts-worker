<?php

namespace AppBundle\Event;

use AppBundle\Entity\Operation;
use Symfony\Component\EventDispatcher\Event;

class OperationSucceedEvent extends Event
{
    const NAME = 'operation.succeed';

    /**
     * @var Operation
     */
    protected $operation;

    public function __construct(Operation $operation)
    {
        $this->operation = $operation;
    }

    /**
     * @return Operation
     */
    public function getOperation(): Operation
    {
        return $this->operation;
    }
}
