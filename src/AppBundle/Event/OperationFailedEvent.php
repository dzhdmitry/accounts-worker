<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class OperationFailedEvent extends Event
{
    const NAME = 'operation.failed';

    /**
     * @var string
     */
    protected $error;

    public function __construct(string $error)
    {
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }
}
