<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table
 * @ORM\Entity
 */
class Operation
{
    const TYPE_DEBIT = 1;
    const TYPE_REFILL = 2;
    const TYPE_TRANSFER = 3;

    const STATUS_APPROVED = 1;
    const STATUS_HOLD = 2;
    const STATUS_CANCELLED = 3;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $operationType;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="operation", cascade={"persist"})
     */
    private $transactions;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    /**
     * @param $type
     * @param Transaction[] $transactions
     * @param int $status
     * @return Operation
     */
    public static function create(int $type, array $transactions, $status = Operation::STATUS_APPROVED): Operation
    {
        $operation = new static();

        $operation->setOperationType($type);
        $operation->setStatus($status);

        foreach ($transactions as $transaction) {
            $operation->addTransaction($transaction);
        }

        return $operation;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $operationType
     * @return Operation
     */
    public function setOperationType(int $operationType): Operation
    {
        $this->operationType = $operationType;

        return $this;
    }

    /**
     * @return int
     */
    public function getOperationType(): int
    {
        return $this->operationType;
    }

    /**
     * @param int $status
     * @return Operation
     */
    public function setStatus(int $status): Operation
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param Transaction $transaction
     * @return Operation
     */
    public function addTransaction(Transaction $transaction): Operation
    {
        $this->transactions[] = $transaction->setOperation($this);

        return $this;
    }

    /**
     * @param Transaction $transaction
     */
    public function removeTransaction(Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * @return ArrayCollection|Transaction[]
     */
    public function getTransactions()
    {
        return $this->transactions;
    }
}
