<?php

namespace AppBundle\DataTransfer;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class OperationApprove
{
    /**
     * @Serializer\Type("int")
     * @Assert\NotNull()
     * @Assert\Type("int")
     */
    public $operation;
}
