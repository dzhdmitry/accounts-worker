<?php

namespace AppBundle\DataTransfer;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class AccountRefill
{
    /**
     * @Serializer\Type("int")
     * @Assert\NotNull()
     * @Assert\Type("int")
     */
    public $account;

    /**
     * @Serializer\Type("float")
     * @Assert\NotNull()
     * @Assert\Type("float")
     * @Assert\GreaterThan(0)
     */
    public $amount;
}
