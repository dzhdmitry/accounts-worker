<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Account;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAccounts extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $first = Account::create('first');
        $second = Account::create('second', 5000);

        $manager->persist($first);
        $manager->persist($second);

        $this->addReference('account-first', $first);
        $this->addReference('account-second', $second);

        $manager->flush();
    }
}
