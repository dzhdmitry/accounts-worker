<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Account;
use AppBundle\Entity\Operation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class TransactionRepository extends EntityRepository
{
    /**
     * @param Account $account
     * @return int
     */
    public function calculateHoldAmount(Account $account): int
    {
        try {
            $results = $this->createQueryBuilder('transaction')
                ->select('SUM(transaction.amount) AS hold')
                ->join('transaction.operation', 'operation')
                ->andWhere('transaction.account = :account')
                ->andWhere('operation.status = :status')
                ->andWhere('transaction.amount < 0')
                ->setParameter('account', $account)
                ->setParameter('status', Operation::STATUS_HOLD)
                ->getQuery()
                ->getSingleResult();

            $hold = $results['hold'] ? $results['hold'] : 0;
        } catch (NoResultException $e) {
            $hold = 0;
        } catch (NonUniqueResultException $e) {
            $hold = 0;
        }

        return $hold;
    }
}
