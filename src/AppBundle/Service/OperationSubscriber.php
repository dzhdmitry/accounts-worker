<?php

namespace AppBundle\Service;

use AppBundle\Event\OperationFailedEvent;
use AppBundle\Event\OperationSucceedEvent;
use Enqueue\Client\ProducerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OperationSubscriber implements EventSubscriberInterface
{
    /**
     * @var ProducerInterface
     */
    private $producer;

    public function __construct(ProducerInterface $producer)
    {
        $this->producer = $producer;
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            'operation.succeed' => 'onOperationSucceed',
            'operation.failed' => 'onOperationFailed'
        ];
    }

    /**
     * @param OperationSucceedEvent $event
     */
    public function onOperationSucceed(OperationSucceedEvent $event)
    {
        $operation = $event->getOperation();

        $this->producer->sendEvent(OperationSucceedEvent::NAME, [
            'message' => sprintf('Operation #%d successfully completed', $operation->getId()),
            'operation' => $operation->getId()
        ]);
    }

    /**
     * @param OperationFailedEvent $event
     */
    public function onOperationFailed(OperationFailedEvent $event)
    {
        $this->producer->sendEvent(OperationFailedEvent::NAME, [
            'message' => 'Operation failed',
            'error' => $event->getError()
        ]);
    }
}
