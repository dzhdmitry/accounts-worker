<?php

namespace AppBundle\Service\Strategy;

use AppBundle\DataTransfer\OperationApprove;
use AppBundle\Entity\Operation;
use AppBundle\Event\OperationSucceedEvent;
use AppBundle\Exception\OperationException;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;

class ApproveStrategy extends OperationStrategy
{
    /**
     * @param OperationApprove $object
     * @throws OptimisticLockException
     * @throws OperationException
     */
    public function execute($object)
    {
        $operation = $this->em->find(Operation::class, $object->operation, LockMode::PESSIMISTIC_WRITE);

        if (!$operation) {
            throw new OperationException('Operation not found');
        }

        $this->operationManager->approve($operation);

        $this->operation = $operation;
    }

    public function onSuccess()
    {
        $event = new OperationSucceedEvent($this->operation);

        $this->dispatcher->dispatch(OperationSucceedEvent::NAME, $event);
    }

    /**
     * @return string
     */
    public function getSupportedDataTransfer()
    {
        return OperationApprove::class;
    }
}
