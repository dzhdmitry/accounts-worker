<?php

namespace AppBundle\Service\Strategy;

use AppBundle\DataTransfer\AccountDebit;
use AppBundle\Entity\Account;
use AppBundle\Entity\Operation;
use AppBundle\Event\OperationSucceedEvent;
use AppBundle\Exception\OperationException;
use AppBundle\Util\MoneyConverter;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;

class DebitStrategy extends AccountStrategy
{
    /**
     * @param AccountDebit $object
     * @throws OptimisticLockException
     * @throws \AppBundle\Exception\OperationException
     */
    public function execute($object)
    {
        $account = $this->em->find(Account::class, $object->account, LockMode::PESSIMISTIC_WRITE);

        if (!$account) {
            throw new OperationException('Account not found');
        }

        $this->operation = $this->accountManager->debit(
            $account,
            MoneyConverter::floatToInternal($object->amount),
            $object->approved ? Operation::STATUS_APPROVED : Operation::STATUS_HOLD
        );
    }

    public function onSuccess()
    {
        $event = new OperationSucceedEvent($this->operation);

        $this->dispatcher->dispatch(OperationSucceedEvent::NAME, $event);
    }

    /**
     * @return string
     */
    public function getSupportedDataTransfer()
    {
        return AccountDebit::class;
    }
}
