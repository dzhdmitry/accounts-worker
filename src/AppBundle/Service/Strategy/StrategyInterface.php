<?php

namespace AppBundle\Service\Strategy;

use AppBundle\Exception\OperationException;

interface StrategyInterface
{
    /**
     * @param object $object
     * @return mixed
     */
    public function execute($object);

    /**
     * @return string
     */
    public function getSupportedDataTransfer();

    public function onSuccess();

    /**
     * @param OperationException $e
     */
    public function onFail(OperationException $e);
}
