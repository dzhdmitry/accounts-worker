<?php

namespace AppBundle\Service\Strategy;

use AppBundle\Entity\Operation;
use AppBundle\Event\OperationFailedEvent;
use AppBundle\Exception\OperationException;
use AppBundle\Service\AccountManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

abstract class AccountStrategy implements StrategyInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var AccountManager
     */
    protected $accountManager;

    /**
     * @var Operation|null
     */
    protected $operation;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, AccountManager $accountManager)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->accountManager = $accountManager;
    }

    /**
     * @param OperationException $e
     */
    public function onFail(OperationException $e)
    {
        $event = new OperationFailedEvent($e->getMessage());

        $this->dispatcher->dispatch(OperationFailedEvent::NAME, $event);
    }
}
