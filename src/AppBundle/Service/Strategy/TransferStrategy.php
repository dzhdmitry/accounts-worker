<?php

namespace AppBundle\Service\Strategy;

use AppBundle\DataTransfer\AccountTransfer;
use AppBundle\Entity\Account;
use AppBundle\Event\OperationSucceedEvent;
use AppBundle\Exception\OperationException;
use AppBundle\Util\MoneyConverter;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;

class TransferStrategy extends AccountStrategy
{
    /**
     * @param AccountTransfer $object
     * @throws OptimisticLockException
     * @throws \AppBundle\Exception\OperationException
     */
    public function execute($object)
    {
        $account = $this->em->find(Account::class, $object->account, LockMode::PESSIMISTIC_WRITE);

        if (!$account) {
            throw new OperationException('Account not found');
        }

        $receiver = $this->em->find(Account::class, $object->receiver, LockMode::PESSIMISTIC_WRITE);

        if (!$receiver) {
            throw new OperationException('Receiver not found');
        }

        $this->operation = $this->accountManager->transfer(
            $account,
            $receiver,
            MoneyConverter::floatToInternal($object->amount)
        );
    }

    public function onSuccess()
    {
        $event = new OperationSucceedEvent($this->operation);

        $this->dispatcher->dispatch(OperationSucceedEvent::NAME, $event);
    }

    /**
     * @return string
     */
    public function getSupportedDataTransfer()
    {
        return AccountTransfer::class;
    }
}
