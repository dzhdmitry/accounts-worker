<?php

namespace AppBundle\Service;

use AppBundle\Exception\OperationException;
use AppBundle\Service\Strategy\StrategyInterface;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Enqueue\Client\TopicSubscriberInterface;
use Interop\Queue\PsrContext;
use Interop\Queue\PsrMessage;
use Interop\Queue\PsrProcessor;
use JMS\Serializer\Exception\Exception as JMSSerializerException;
use JMS\Serializer\Serializer;
use Monolog\Logger;
use Symfony\Component\Serializer\Exception\ExceptionInterface as SymfonySerializerException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MessageProcessor implements PsrProcessor, TopicSubscriberInterface
{
    const MESSAGE_CONTENT_TYPE = 'json';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var StrategyInterface[]
     */
    private $strategy = [];

    public function __construct(EntityManager $em, Logger $logger, Serializer $serializer, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @param PsrMessage $message
     * @param PsrContext $context
     *
     * @return string|object with __toString method implemented
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     */
    public function process(PsrMessage $message, PsrContext $context)
    {
        $operation = $message->getProperty('enqueue.topic_name');
        $action = $this->getStrategy($operation);

        if (!$action) {
            return self::REJECT;
        }

        try {
            $object = $this->serializer->deserialize(
                $message->getBody(),
                $action->getSupportedDataTransfer(),
                self::MESSAGE_CONTENT_TYPE
            );
        } catch (JMSSerializerException $e) {
            return self::REJECT;
        } catch (SymfonySerializerException $e) {
            return self::REJECT;
        }

        $errors = $this->validator->validate($object);

        if ($errors->count()) {
            return self::REJECT;
        }

        $this->em->getConnection()->beginTransaction();

        try {
            try {
                $action->execute($object);
                $this->em->getConnection()->commit();
                $action->onSuccess();
            } catch (OperationException $e) {
                $this->em->getConnection()->rollBack();
                $action->onFail($e);
            } catch (OptimisticLockException $e) {
                $this->em->getConnection()->rollBack();
                $this->logError($e);
            } catch (\Throwable $e) {
                if ($this->em->getConnection()->isTransactionActive()) {
                    $this->em->getConnection()->rollBack();
                }

                $this->logError($e);
            }
        } catch (ConnectionException $e) {
            $this->logError($e);
        }

        $this->em->clear();
        $this->em->getConnection()->close();

        return self::ACK;
    }

    /**
     * @return array
     */
    public static function getSubscribedTopics()
    {
        return [
            'account:refill',
            'account:debit',
            'account:transfer',
            'operation:approve',
            'operation:cancel'
        ];
    }

    /**
     * @param string $name
     * @param StrategyInterface $action
     */
    public function addStrategy($name, StrategyInterface $action)
    {
        $this->strategy[$name] = $action;
    }

    /**
     * @param string $name
     * @return StrategyInterface|null
     */
    private function getStrategy($name)
    {
        if (!array_key_exists($name, $this->strategy)) {
            return null;
        }

        return $this->strategy[$name];
    }

    /**
     * @param \Throwable $e
     */
    private function logError(\Throwable $e)
    {
        $this->logger->log(Logger::CRITICAL, sprintf('Error during transaction: %s', $e->getMessage()));
    }
}
