<?php

namespace AppBundle\Service;

use AppBundle\Entity\Operation;
use AppBundle\Exception\OperationException;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class OperationManager
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Approve an operation
     *
     * @param Operation $operation
     * @throws OperationException
     */
    public function approve(Operation $operation)
    {
        if (!$this->canBeApproved($operation)) {
            throw new OperationException('Operation cannot be approved');
        }

        $operation->setStatus(Operation::STATUS_APPROVED);

        $this->applyOperation($operation);

        $this->em->flush();
    }

    /**
     * Cancel an operation
     *
     * @param Operation $operation
     * @throws OperationException
     */
    public function cancel(Operation $operation)
    {
        if (!$this->canBeCancelled($operation)) {
            throw new OperationException('Operation cannot be cancelled');
        }

        $operation->setStatus(Operation::STATUS_APPROVED);

        $this->em->flush();
    }

    /**
     * @param Operation $operation
     */
    public function applyOperation(Operation $operation)
    {
        if ($operation->getStatus() !== Operation::STATUS_APPROVED) {
            return;
        }

        foreach ($operation->getTransactions() as $transaction) {
            $account = $transaction->getAccount();

            $this->em->lock($account, LockMode::PESSIMISTIC_WRITE);

            $account->setBalance($account->getBalance() + $transaction->getAmount());
        }
    }

    /**
     * @param Operation $operation
     * @return bool
     */
    private function canBeApproved(Operation $operation): bool
    {
        return $operation->getStatus() === Operation::STATUS_HOLD;
    }

    /**
     * @param Operation $operation
     * @return bool
     */
    private function canBeCancelled(Operation $operation): bool
    {
        return $operation->getStatus() === Operation::STATUS_HOLD;
    }
}
