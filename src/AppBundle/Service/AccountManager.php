<?php

namespace AppBundle\Service;

use AppBundle\Entity\Account;
use AppBundle\Entity\Operation;
use AppBundle\Entity\Transaction;
use AppBundle\Exception\OperationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class AccountManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var OperationManager
     */
    private $operationManager;

    public function __construct(EntityManagerInterface $em, OperationManager $operationManager)
    {
        $this->em = $em;
        $this->operationManager = $operationManager;
    }

    /**
     * Make REFILL operation with an account
     *
     * @param Account $account
     * @param int $amount [internal]
     * @return Operation
     */
    public function refill(Account $account, int $amount): Operation
    {
        $transaction = Transaction::create($account, $amount);
        $operation = Operation::create(Operation::TYPE_REFILL, [$transaction]);

        $this->operationManager->applyOperation($operation);

        $this->em->persist($transaction);
        $this->em->flush();

        return $operation;
    }

    /**
     * Make DEBIT operation with an account
     *
     * @param Account $account
     * @param int $amount [internal]
     * @param int $status
     * @return Operation
     * @throws OperationException
     */
    public function debit(Account $account, int $amount, $status = Operation::STATUS_APPROVED): Operation
    {
        if (!$this->debitIsAllowedByBalance($account, $amount)) {
            throw new OperationException('Insufficient funds on the account');
        }

        if (!$this->debitIsAllowedByHoldOperations($account, $amount)) {
            throw new OperationException('Insufficient funds on the account: Account has operations on hold');
        }

        $transaction = Transaction::create($account, $amount * -1);
        $operation = Operation::create(Operation::TYPE_DEBIT, [$transaction], $status);

        $this->operationManager->applyOperation($operation);

        $this->em->persist($transaction);
        $this->em->flush();

        return $operation;
    }

    /**
     * Make TRANSFER operation with an account
     *
     * @param Account $account
     * @param Account $receiver
     * @param int $amount [internal]
     * @return Operation
     * @throws OperationException
     */
    public function transfer(Account $account, Account $receiver, int $amount): Operation
    {
        if (!$this->debitIsAllowedByBalance($account, $amount)) {
            throw new OperationException('Insufficient funds on the account');
        }

        if (!$this->debitIsAllowedByHoldOperations($account, $amount)) {
            throw new OperationException('Insufficient funds on the account: Account has operations on hold');
        }

        // debit for account
        $debit = Transaction::create($account, $amount * -1);

        // refill for receiver
        $refill = Transaction::create($receiver, $amount);

        // operation
        $operation = Operation::create(Operation::TYPE_TRANSFER, [$debit, $refill]);

        $this->operationManager->applyOperation($operation);

        $this->em->persist($debit);
        $this->em->persist($refill);
        $this->em->flush();

        return $operation;
    }

    /**
     * @param Account $account
     * @param int $amount [internal]
     * @return bool
     */
    private function debitIsAllowedByBalance(Account $account, int $amount): bool
    {
        return $account->getBalance() - $amount >= 0;
    }

    /**
     * @param Account $account
     * @param int $amount [internal]
     * @return bool
     */
    private function debitIsAllowedByHoldOperations(Account $account, int $amount): bool
    {
        $hold = $this->em->getRepository(Transaction::class)->calculateHoldAmount($account);

        return $account->getBalance() + $hold - $amount >= 0;
    }
}
