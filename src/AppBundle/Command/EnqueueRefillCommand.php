<?php

namespace AppBundle\Command;

use Enqueue\Client\ProducerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EnqueueRefillCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:enqueue:refill')
            ->setDescription('REFILL on the account')
            ->addArgument('account', InputArgument::REQUIRED, 'ID of the account')
            ->addArgument('amount', InputArgument::REQUIRED, 'Amount of refill')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $id = $input->getArgument('account');
        $amount = $input->getArgument('amount');

        $container->get(ProducerInterface::class)->sendEvent('account:refill', [
            'account' => intval($id),
            'amount' => floatval($amount)
        ]);
    }
}
