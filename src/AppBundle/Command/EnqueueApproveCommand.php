<?php

namespace AppBundle\Command;

use Enqueue\Client\Message;
use Enqueue\Client\ProducerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EnqueueApproveCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:enqueue:approve')
            ->setDescription('APPROVE operation')
            ->addArgument('operation', InputArgument::REQUIRED, 'ID of the operation')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $id = $input->getArgument('operation');

        $container->get(ProducerInterface::class)->sendEvent('operation:approve', [
            'operation' => intval($id)
        ]);
    }
}
