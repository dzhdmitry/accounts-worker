<?php

namespace AppBundle\Command;

use Enqueue\Client\ProducerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class EnqueueDebitCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:enqueue:debit')
            ->setDescription('DEBIT on the account')
            ->addArgument('account', InputArgument::REQUIRED, 'ID of the account')
            ->addArgument('amount', InputArgument::REQUIRED, 'Amount of debit')
            ->addOption('hold', null, InputOption::VALUE_REQUIRED, 'Approve [default] or hold debit', 'no')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $id = $input->getArgument('account');
        $amount = $input->getArgument('amount');
        $approved = !($input->getOption('hold') === 'yes');

        $container->get(ProducerInterface::class)->sendEvent('account:debit', [
            'account' => intval($id),
            'amount' => floatval($amount),
            'approved' => $approved
        ]);
    }
}
