<?php

namespace AppBundle\Command;

use Enqueue\Client\ProducerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EnqueueTransferCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:enqueue:transfer')
            ->setDescription('TRANSFER between accounts')
            ->addArgument('account', InputArgument::REQUIRED, 'ID of the account')
            ->addArgument('amount', InputArgument::REQUIRED, 'Amount of transfer')
            ->addArgument('receiver', InputArgument::REQUIRED, 'ID of receiver account')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $id = $input->getArgument('account');
        $amount = $input->getArgument('amount');
        $receiverId = $input->getArgument('receiver');

        $container->get(ProducerInterface::class)->sendEvent('account:transfer', [
            'account' => intval($id),
            'amount' => floatval($amount),
            'receiver' => intval($receiverId)
        ]);
    }
}
