<?php

namespace AppBundle\Command;

use AppBundle\Entity\Account;
use AppBundle\Util\MoneyConverter;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateAccountCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:account:create')
            ->setDescription('Create new account')
            ->addArgument('name', InputArgument::REQUIRED, 'Name of the account')
            ->addArgument('balance', InputArgument::OPTIONAL, 'Initial balance', 0)
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $name = $input->getArgument('name');
        $balance = MoneyConverter::stringToInternal($input->getArgument('balance'));
        $account = Account::create($name, $balance);

        $em->persist($account);
        $em->flush();
    }
}
