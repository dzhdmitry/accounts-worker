# Приложение-микросервис баланса пользователей

Сервис сделан на symfony-приложении, которое слушает очередь в RabbitMQ, хранит все данные в MySQL.
Сделаны операции списания, зачисления (с блокированием и без), подтверждение/отмена заблокирванного зачисления.
После совершения операции в очередь пишется Event.

Unit-тесты не написал, взамен них есть функциональные тесты.

## Как проверить операции

В проекте есть несколько консольных коменд для проверки работоспособности приложения (см /src/AppBundle/Command).
После запуска consumer-а можно запустить операцию, например залисление на счет:

```
php bin/console app:enqueue:refill 1 100
```

(переведет 100 условных единиц на счет с ID=1)

## Перечень используемых инструментов 

В проекте используются:

- PHP 7.2
- MySQL 5.7
- RabbitMQ 3.7
- Symfony 3.4

## Способы развертки приложения

### Вручную

1. Установить PHP 7.2 (ext-mysql, ext-amqp, ext-zip), MySQL 5.7, RabbitMQ 3.7, composer

### Docker-compose

1 Скачать конфигурацию [https://bitbucket.org/dzhdmitry/accounts-worker-docker](https://bitbucket.org/dzhdmitry/accounts-worker-docker)

2 Скопировать файл `/.env.dist` в `/.env` и указать там путь до проекта

3 Добавить в файл hosts (IP - ip-адрес в локальной сети или 127.0.0.1)

```
<IP> db
<IP> mq
```

4 Запустить

```
docker-compose build
docker-compose up -d
```

## Первый запуск

1 Запустить

```
composer install
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
php bin/console cache:clear
```

2 Запустить subscriber очереди, например `php bin/console enqueue:consume --setup-broker`

## Тесты

1 Создать БД для тестового окружения
```
php bin/console doctrine:database:create --env=test
php bin/console doctrine:schema:create --env=test
```

2 Запустить subscriber очереди в тестовом режиме, например `php bin/console enqueue:consume -vvv --setup-broker --time-limit="60 seconds" --env=test`

3 Запустить тесты `vendor/bin/simple-phpunit`
