<?php

namespace Tests\AppBundle\Command;

class EnqueueTransferCommandTest extends CommandTest
{
    public function testSuccess()
    {
        $first = $this->fixtures->getReference('account-first');
        $second = $this->fixtures->getReference('account-second');

        // Transfer 50 from second to first
        $this->runCommand('app:enqueue:transfer', [
            'account' => $second->getId(),
            'amount' => 50,
            'receiver' => $first->getId()
        ]);

        sleep(1);

        $this->assertAccountHasBalance($first->getId(), 5000);
        $this->assertAccountHasBalance($second->getId(), 0);
    }

    public function testFail()
    {
        $first = $this->fixtures->getReference('account-first');
        $second = $this->fixtures->getReference('account-second');

        // Transfer 1 from first to second
        $this->runCommand('app:enqueue:transfer', [
            'account' => $first->getId(),
            'amount' => 1,
            'receiver' => $second->getId()
        ]);

        sleep(1);

        $this->assertAccountHasBalance($first->getId(), 0);
        $this->assertAccountHasBalance($second->getId(), 5000);
    }
}
