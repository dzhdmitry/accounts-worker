<?php

namespace Tests\AppBundle\Command;

class EnqueueDebitCommandTest extends CommandTest
{
    public function testApprovedSuccess()
    {
        $second = $this->fixtures->getReference('account-second');

        // Debit 20.00 account having 50.00
        $this->runCommand('app:enqueue:debit', [
            'account' => $second->getId(),
            'amount' => 20
        ]);

        sleep(1);

        $this->assertAccountHasBalance($second->getId(), 3000);

        // Debit 30.00 account having 30.00
        $this->runCommand('app:enqueue:debit', [
            'account' => $second->getId(),
            'amount' => 30
        ]);

        sleep(1);

        $this->assertAccountHasBalance($second->getId(), 0);
    }

    public function testApprovedFail()
    {
        $first = $this->fixtures->getReference('account-first');
        $second = $this->fixtures->getReference('account-second');

        // Debit 1.00 account having 0.00
        $this->runCommand('app:enqueue:debit', [
            'account' => $first->getId(),
            'amount' => 1
        ]);

        sleep(1);

        $this->assertAccountHasBalance($first->getId(), 0);

        // Debit 51.00 account having 50.00
        $this->runCommand('app:enqueue:debit', [
            'account' => $second->getId(),
            'amount' => 51
        ]);

        sleep(1);

        $this->assertAccountHasBalance($second->getId(), 5000);
    }

    public function testHoldSuccess()
    {
        $second = $this->fixtures->getReference('account-second');

        // Debit [hold] 20.00 account having 50.00
        $this->runCommand('app:enqueue:debit', [
            'account' => $second->getId(),
            'amount' => 20,
            'hold' => 'yes'
        ]);

        sleep(1);

        $this->assertAccountHasBalance($second->getId(), 5000);

        // Debit 10.00 account having 50.00 and 20.00 on hold
        $this->runCommand('app:enqueue:debit', [
            'account' => $second->getId(),
            'amount' => 10
        ]);

        sleep(1);

        $this->assertAccountHasBalance($second->getId(), 4000);

        // Debit [hold] 20.00 account having 40.00 and 20.00 on hold
        $this->runCommand('app:enqueue:debit', [
            'account' => $second->getId(),
            'amount' => 20,
            'hold' => 'yes'
        ]);

        sleep(1);

        $this->assertAccountHasBalance($second->getId(), 4000);
    }

    public function testHoldFail()
    {
        $first = $this->fixtures->getReference('account-first');
        $second = $this->fixtures->getReference('account-second');

        // Debit [hold] 1.00 account having 0.00
        $this->runCommand('app:enqueue:debit', [
            'account' => $first->getId(),
            'amount' => 1,
            'hold' => 'yes'
        ]);

        sleep(1);

        $this->assertAccountHasBalance($first->getId(), 0);

        // Debit [hold] 51.00 account having 50.00
        $this->runCommand('app:enqueue:debit', [
            'account' => $second->getId(),
            'amount' => 51,
            'hold' => 'yes'
        ]);

        sleep(1);

        $this->assertAccountHasBalance($second->getId(), 5000);

        // Debit [hold] 30.00 account having 50.00
        $this->runCommand('app:enqueue:debit', [
            'account' => $second->getId(),
            'amount' => 30,
            'hold' => 'yes'
        ]);

        sleep(1);

        // Debit [hold] 30.00 account having 50.00 and 30.00 on hold
        $this->runCommand('app:enqueue:debit', [
            'account' => $second->getId(),
            'amount' => 30,
            'hold' => 'yes'
        ]);

        sleep(1);

        $this->assertAccountHasBalance($second->getId(), 5000);
    }
}
