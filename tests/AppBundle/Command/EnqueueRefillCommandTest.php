<?php

namespace Tests\AppBundle\Command;

class EnqueueRefillCommandTest extends CommandTest
{
    public function testSuccess()
    {
        $first = $this->fixtures->getReference('account-first');
        $second = $this->fixtures->getReference('account-second');

        // Refill 100.00 empty account
        $this->runCommand('app:enqueue:refill', [
            'account' => $first->getId(),
            'amount' => 100
        ]);

        sleep(1);

        $this->assertAccountHasBalance($first->getId(), 10000);

        // Refill 100.00 empty account having 50.00 balance
        $this->runCommand('app:enqueue:refill', [
            'account' => $second->getId(),
            'amount' => 100
        ]);

        sleep(1);

        $this->assertAccountHasBalance($second->getId(), 15000);
    }
}
