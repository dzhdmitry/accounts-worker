<?php

namespace Tests\AppBundle\Command;

use AppBundle\DataFixtures\ORM\LoadAccounts;
use AppBundle\Entity\Account;
use Doctrine\Common\DataFixtures\ProxyReferenceRepository;
use Doctrine\ORM\EntityManager;
use Enqueue\Client\TraceableProducer;
use Liip\FunctionalTestBundle\Test\WebTestCase;

abstract class CommandTest extends WebTestCase
{
    /**
     * @var EntityManager
     */
    protected static $em;

    /**
     * @var ProxyReferenceRepository
     */
    protected $fixtures;

    public function setUp()
    {
        static::bootKernel();

        static::$em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');

        $this->fixtures = $this->loadFixtures([
            LoadAccounts::class
        ])->getReferenceRepository();

        parent::setUp();
    }

    /**
     * @param $id
     * @param $expectedBalance
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public static function assertAccountHasBalance($id, $expectedBalance)
    {
        $actual = static::$em->getRepository(Account::class)
            ->createQueryBuilder('account')
            ->select('account.balance as balance')
            ->andWhere('account.id = :id')
            ->setMaxResults(1)
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();

        static::assertEquals($expectedBalance, intval($actual));
    }

    public static function getResponseJsonContent()
    {
        $producer = static::$kernel->getContainer()->get(TraceableProducer::class);

        $x = $producer->getTraces();

        //var_dump($x);
    }
}
